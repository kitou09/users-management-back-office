from django.urls import include, path
from rest_framework import routers
from .views import UserView, LogoutView, RefreshTokenView, LoginView, profile_view, HometownView

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'users', UserView, basename='UserView')
router.register(r'hometowns', HometownView, basename='HometownView')

urlpatterns = [
    path('api/v1/login', LoginView.as_view()),
    path('api/v1/logout', LogoutView.as_view()),
    path('api/v1/refresh-token', RefreshTokenView.as_view(), name='refresh_token'),
    path('api/v1/profile', profile_view),
    path('api/v1/', include(router.urls))
]
