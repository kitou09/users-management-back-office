from django_filters import rest_framework as filters
from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from .serializers import UserSerializer, ChangePasswordSerializer, MyTokenObtainPairSerializer, \
    TokenRefreshSerializer, ProfileSerializer, HometownSerializer
from globals.helpers import format_form_errors_response
from .filters import UserFilter, HometownFilter
from globals.ordering import CustomOrdering
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView
from .models import User, Hometown
from rest_framework import filters as rest_filters


@api_view()
def error_404_page(request, exception=None):
    return Response({'errors': {'message': _('Page not found')}}, status=status.HTTP_404_NOT_FOUND)


class RefreshTokenView(TokenRefreshView):
    serializer_class = TokenRefreshSerializer


class LoginView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class LogoutView(TokenRefreshView):
    serializer_class = TokenRefreshSerializer

    def post(self, request, *args):
        sz = self.get_serializer(data=request.data)
        sz.is_valid(raise_exception=True)
        return Response(status=status.HTTP_200_OK)


# View For User.
class UserView(viewsets.ModelViewSet):
    """
    Provides a get method handler.
    """
    renderer_classes = [JSONRenderer]
    serializer_class = UserSerializer
    filter_backends = (filters.DjangoFilterBackend, CustomOrdering, rest_filters.SearchFilter)
    filterset_class = UserFilter
    permission_classes = (IsAuthenticated,)
    allowed_custom_filters = ['id', 'username', 'created']
    fields_related = {
        'id': 'id',
        'username': 'username',
        'created': 'created',
    }
    search_fields = ['username', 'first_name', 'last_name']

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        group = []
        if self.action == 'me':
            group = ['me']
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self,
            'group': group,
            'user': self.request.user
        }

    def get_queryset(self):
        return User.objects.all()

    def create(self, request, *args, **kwargs):
        form = self.get_serializer(data=request.data)
        if form.is_valid():
            instance = form.save()
            serializer = self.get_serializer(instance, many=False)
            return Response(
                data=serializer.data,
                status=status.HTTP_201_CREATED
            )
        return Response(
            data=format_form_errors_response(form.errors),
            status=status.HTTP_400_BAD_REQUEST
        )

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        form = self.get_serializer(instance, data=request.data, partial=True)
        if form.is_valid():
            new_instance = form.save()
            serializer = self.get_serializer(new_instance, many=False)
            return Response(
                data=serializer.data,
                status=status.HTTP_200_OK
            )
        return Response(
            data=format_form_errors_response(form.errors),
            status=status.HTTP_400_BAD_REQUEST
        )

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=False, methods=['get'])
    def me(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user, many=False)
        return Response(serializer.data)

    @action(detail=False, methods=['put'], url_path='change-password')
    def change_password(self, request, *args, **kwargs):
        user = request.user
        change_password = ChangePasswordSerializer(data=request.data or {})
        if change_password.is_valid():
            passwords = change_password.data
            if user.check_password(passwords['old_password']):
                user.set_password(passwords['password'])
                user.save()
                serializer = self.get_serializer(user, many=False)
                return Response(
                    data=serializer.data,
                    status=status.HTTP_200_OK
                )
            errors = {"old_password": ["Wrong password."]}
        else:
            errors = change_password.errors
        return Response(
            data=format_form_errors_response(errors),
            status=status.HTTP_400_BAD_REQUEST
        )

    @action(detail=False, methods=['post'], url_path='username/exists')
    def check_username(self, request, *args, **kwargs):
        data = request.data
        errors = {"username": ["This field is required."]}
        if 'username' in data:
            users = User.objects.filter(username=data['username'])
            if users:
                return Response(status=status.HTTP_200_OK)
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(
            data=format_form_errors_response(errors),
            status=status.HTTP_400_BAD_REQUEST
        )


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def profile_view(request):
    user = request.user
    if not isinstance(user, User):
        return Response(data={'message': 'Profile not found'}, status=status.HTTP_404_NOT_FOUND)
    profile = user.profile
    profile_form = ProfileSerializer(instance=profile, data=request.data or {})
    if profile_form.is_valid():
        profile = profile_form.save()
        serializer = ProfileSerializer(profile, many=False)
        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK
        )
    return Response(
        data=format_form_errors_response(profile_form.errors),
        status=status.HTTP_400_BAD_REQUEST
    )


# View For Hometown.
class HometownView(viewsets.ModelViewSet):
    """
    Provides a get method handler.
    """
    renderer_classes = [JSONRenderer]
    serializer_class = HometownSerializer
    filter_backends = (filters.DjangoFilterBackend, CustomOrdering, rest_filters.SearchFilter)
    filterset_class = HometownFilter
    queryset = Hometown.objects.filter(deleted=False)
    permission_classes = (IsAuthenticated,)
    allowed_custom_filters = ['id', 'name']
    fields_related = {
        'id': 'id',
        'name': 'name',
    }
    search_fields = ['name']

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            instance.delete()
        except Exception as ex:
            instance.deleted = True
            instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
