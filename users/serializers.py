from django.contrib.auth.models import update_last_login
from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken

from .constants import GENDERS
from .models import User, Profile, Hometown
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from api.settings import SIMPLE_JWT as SIMPLE_JWT_SETTING
from django.utils.translation import ugettext_lazy as _


class TokenRefreshSerializer(serializers.Serializer):
    refresh_token = serializers.CharField()

    def validate(self, attrs):
        refresh_token = RefreshToken(attrs['refresh_token'])

        data = {'access_token': str(refresh_token.access_token)}

        if SIMPLE_JWT_SETTING['ROTATE_REFRESH_TOKENS']:
            if SIMPLE_JWT_SETTING['BLACKLIST_AFTER_ROTATION']:
                try:
                    # Attempt to blacklist the given refresh token
                    refresh_token.blacklist()
                except AttributeError:
                    # If blacklist app not installed, `blacklist` method will
                    # not be present
                    pass

            refresh_token.set_jti()
            refresh_token.set_exp()

        data['refresh_token'] = str(refresh_token)

        return data


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):
        data = super().validate(attrs)
        data['refresh_token'] = data['refresh']
        data['access_token'] = data['access']
        del data['access']
        del data['refresh']
        if SIMPLE_JWT_SETTING['UPDATE_LAST_LOGIN']:
            update_last_login(None, self.user)
        return data


class RelatedFieldAlternative(serializers.PrimaryKeyRelatedField):
    default_error_messages = {
        'required': _('This field is required.'),
        'does_not_exist': _('This value {pk_value} is not a valid choice.'),
        'incorrect_type': _('Incorrect type. Expected id value, received {data_type}.'),
    }

    def __init__(self, context, **kwargs):
        self.serializer = kwargs.pop('serializer', None)
        self.myContext = context
        if self.serializer is not None and not issubclass(self.serializer, serializers.Serializer):
            raise TypeError('"serializer" is not a valid serializer class')
        super().__init__(**kwargs)

    def use_pk_only_optimization(self):
        return False if self.serializer else True

    def to_representation(self, instance):
        if self.serializer:
            context = self.myContext if self.myContext else self.context
            return self.serializer(instance, context=context).data
        return super().to_representation(instance)


class ProfileSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(ProfileSerializer, self).__init__(*args, **kwargs)

        if 'context' in kwargs and 'group' in kwargs['context']:
            if 'remove_fields' in kwargs['context']['group']:
                del self.fields['last_name']
                del self.fields['updated']

    class Meta:
        model = Profile
        fields = '__all__'
        depth = 1


class HometownSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hometown
        fields = ['id', 'name']


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(many=False, required=False)
    last_login = serializers.DateTimeField(read_only=True)
    age = serializers.IntegerField(required=True, write_only=True)
    gender = serializers.ChoiceField(write_only=True, choices=GENDERS)
    hometown = RelatedFieldAlternative(queryset=Hometown.objects.all(),
                                       serializer=HometownSerializer, required=True, context={}, write_only=True)

    def __init__(self, *args, **kwargs):
        super(UserSerializer, self).__init__(*args, **kwargs)

        if 'context' in kwargs and 'group' in kwargs['context']:
            if 'remove_fields' in kwargs['context']['group']:
                del self.fields['is_active']
                del self.fields['last_login']
                del self.fields['created']

    def create(self, validated_data):
        user = User(username=validated_data.get('username'), is_active=validated_data.get('is_active', None),
                    first_name=validated_data.get('first_name'), last_name=validated_data.get('last_name', None))
        user.set_password(validated_data.get('password'))

        profile = Profile.objects.create(age=validated_data.get('age'),
                                         gender=validated_data.get('gender'),
                                         hometown=validated_data.get('hometown'))
        user.profile = profile
        user.save()
        return user

    def update(self, instance, validated_data):
        profile = instance.profile
        password = validated_data.get('password', None)
        if password:
            instance.set_password(password)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.save()

        profile.age = validated_data.get('age', profile.age)
        profile.gender = validated_data.get('gender', profile.gender)
        profile.hometown = validated_data.get('hometown', profile.hometown)
        profile.save()

        return instance

    class Meta:
        model = User
        fields = ['id', 'username', 'is_active', 'first_name', 'last_name', 'password', 'age', 'gender', 'hometown',
                  'profile', 'last_login', 'created']
        extra_kwargs = {'password': {'write_only': True}, 'created': {'read_only': True}}
        depth = 1


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
