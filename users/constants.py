from django.utils.translation import ugettext_lazy as _

MALE = 'Male'
FEMALE = 'Female'
OPERATORS = ['lte', 'lt', 'gt', 'gte', 'between', 'exact']

GENDERS = (
    (MALE, _(MALE)),
    (FEMALE, _(FEMALE)),
)
