from django.db import models

from .constants import GENDERS
from .managers import UserManager, BaseManager
from django.contrib.auth.models import AbstractUser
from django.utils import timezone


class Hometown(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, blank=False, unique=True)
    deleted = models.BooleanField(default=False, blank=True, null=True)

    objects = BaseManager()

    class Meta:
        db_table = 'hometown'
        ordering = ['-id']


class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    hometown = models.ForeignKey(Hometown, on_delete=models.PROTECT)
    age = models.IntegerField(null=False, blank=False)
    gender = models.CharField(max_length=6, blank=False, null=False, choices=GENDERS)
    updated = models.DateTimeField(auto_now=True, db_column='updated')

    objects = BaseManager()

    class Meta:
        db_table = 'profile'
        ordering = ['id']


class User(AbstractUser):
    first_name = models.CharField(max_length=250, null=False, blank=False, db_column='first_name')
    last_name = models.CharField(max_length=250, null=True, blank=True, db_column='last_name')
    username = models.CharField(db_index=True, max_length=150, unique=True, null=False)
    profile = models.OneToOneField(Profile, on_delete=models.CASCADE)
    created = models.DateTimeField(default=timezone.now, db_column='created')
    updated = models.DateTimeField(auto_now=True, db_column='updated')

    USERNAME_FIELD = 'username'

    objects = UserManager()

    class Meta:
        db_table = 'user'
        ordering = ['-id']
