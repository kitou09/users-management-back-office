from ast import literal_eval

from django_filters import rest_framework as filters

from .constants import OPERATORS
from .models import User, Hometown


class UserFilter(filters.FilterSet):
    username = filters.CharFilter(lookup_expr='icontains')
    is_active = filters.BooleanFilter(lookup_expr='exact', field_name="is_active")
    first_name = filters.CharFilter(lookup_expr='icontains')
    last_name = filters.CharFilter(lookup_expr='icontains')
    p_id = filters.NumberFilter(field_name='profile__id', lookup_expr='exact')
    p_age = filters.CharFilter(field_name='profile__age', method='custom_age_filter')
    p_gender = filters.CharFilter(field_name='profile__gender', lookup_expr='iexact')
    p_hometown = filters.CharFilter(field_name='profile__hometown__id', lookup_expr='icontains')

    def custom_age_filter(self, queryset, name, value):
        if not value:
            return queryset
        try:
            kwargs = {}
            values = literal_eval(value)
            if ("operator" in values) and ("content" in values):
                operator = values['operator']
                content = values['content']
                if operator in OPERATORS:
                    if operator != 'between' and ("start" in content):
                        kwargs['{0}__{1}'.format(name, operator)] = content['start']
                    elif ("start" in content) and ("end" in content):
                        kwargs['{0}__lte'.format(name)] = content['end']
                        kwargs['{0}__gte'.format(name)] = content['start']
            if kwargs:
                queryset = queryset.filter(**kwargs)
        except Exception as ex:
            pass

        return queryset

    class Meta:
        model = User
        fields = {
            'id': ['exact'],
        }


class HometownFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Hometown
        fields = {
            'id': ['exact'],
        }
