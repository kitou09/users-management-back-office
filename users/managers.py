from django.contrib.auth.base_user import BaseUserManager
from django.db import models


class UserManager(BaseUserManager):
    pass


class BaseManager(models.Manager):
    pass
