# Installation
### To install requirements

<code>pip install -r requirements.txt</code>

### To run server

<code>python manage.py runserver</code>

You don't need to run migrations and migrate commands because is already done.